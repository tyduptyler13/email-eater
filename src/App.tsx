import React, {
  FormEventHandler,
  useCallback,
  useEffect,
  useMemo,
  useReducer,
  useRef,
} from "react";
import {
  createScheduler,
  createWorker,
  Line,
  OEM,
  RecognizeResult,
  Worker,
} from "tesseract.js";
import {
  AppBar,
  Button,
  Card,
  CardActions,
  CardContent,
  Container,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Toolbar,
  Typography,
} from "@mui/material";
import { Camera, FileCopy } from "@mui/icons-material";

interface Doc {
  file: File;
  objectUrl: string;
  loading: boolean;
  matches: {
    email: string;
    confidence: number;
  }[];
}

function App() {
  const scheduler = useMemo(() => createScheduler(), []);

  useEffect(() => {
    createWorker(
      "eng",
      OEM.DEFAULT,
      {},
      { load_freq_dawg: "false", load_system_dawg: "false" },
    ).then((worker: Worker) => scheduler.addWorker(worker));

    return () => {
      scheduler.terminate();
    };
  }, [scheduler]);

  const documents = useRef<Doc[]>([]);
  const [, forceUpdate] = useReducer((x) => x + 1, 0);

  const handleNewImage = useCallback<FormEventHandler<HTMLInputElement>>(
    (e) => {
      const files: FileList | [] = (e.target as HTMLInputElement).files ?? [];
      if (files.length > 0) {
        const document: Doc = {
          file: files[0],
          objectUrl: URL.createObjectURL(files[0]),
          loading: true,
          matches: [],
        };

        documents.current.push(document);
        forceUpdate();

        scheduler
          .addJob("recognize", files[0])
          .then((result: RecognizeResult) => {
            const emailMatcher = /\S+@\S+\.\S+/g;

            document.matches = result.data.lines.flatMap(
              (line: Line) =>
                line.text.match(emailMatcher)?.map((match: string) => ({
                  email: match,
                  confidence: line.confidence,
                })) ?? [],
            );
          })
          .finally(() => {
            document.loading = false;
            forceUpdate();
          });
      }
    },
    [documents, scheduler],
  );

  const copyEmails = useCallback(() => {
    navigator.clipboard.writeText(
      documents.current
        .flatMap((document) => document.matches.map((match) => match.email))
        .join("\n"),
    );
  }, []);

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h1">Email Eater</Typography>
        </Toolbar>
      </AppBar>
      <Container component="main">
        <Typography paragraph>
          Hi Nancy, click the file upload input and choose to take a picture,
          unfortunately you have to do this per page. Eventually safari will
          support me capturing snapshots from your camera live, but this is the
          best I can do for iphone. Every page you upload will be scanned and
          the emails will be saved to a table. When you are done, click the copy
          button, you will be able to send that table/list to whatever document
          or email you want.
        </Typography>
        <Typography paragraph>
          If any of the emails are low confidence I recommend taking a look and
          fixing those manually.
        </Typography>
        <Card>
          <CardContent>
            {documents.current.length > 0 ? (
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Document</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell>Confidence</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {documents.current.map((doc) => (
                    <>
                      <TableRow key={doc.file.name}>
                        <TableCell rowSpan={Math.max(doc.matches.length, 1)}>
                          <a
                            href={doc.objectUrl}
                            target="_blank"
                            rel="noreferrer"
                          >
                            {doc.file.name}
                          </a>
                        </TableCell>
                        {doc.loading ? (
                          <TableCell rowSpan={2}>Loading</TableCell>
                        ) : doc.matches.length === 0 ? (
                          <TableCell rowSpan={2}>No email found</TableCell>
                        ) : (
                          <>
                            <TableCell>{doc.matches[0].email}</TableCell>
                            <TableCell>{doc.matches[0].confidence}</TableCell>
                          </>
                        )}
                      </TableRow>
                      {doc.matches.slice(1).map((match, index) => (
                        <TableRow key={`${doc.file.name}_${index}`}>
                          <TableCell>{match.email}</TableCell>
                          <TableCell>{match.confidence}</TableCell>
                        </TableRow>
                      ))}
                    </>
                  ))}
                </TableBody>
              </Table>
            ) : (
              <span>No documents have been uploaded</span>
            )}
          </CardContent>
          <CardActions>
            <Button component="label" variant="outlined" endIcon={<Camera />}>
              <input
                hidden
                type="file"
                accept="image/*"
                onInput={handleNewImage}
              />
              Upload/Capture
            </Button>
            <Button onClick={copyEmails} endIcon={<FileCopy />}>
              Copy Emails
            </Button>
          </CardActions>
        </Card>
      </Container>
    </>
  );
}

export default App;
